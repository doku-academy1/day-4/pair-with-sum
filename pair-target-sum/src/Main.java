import java.util.*;

public class Main {
    public static void main(String[] args) {

        Integer[] numbers = new Integer[] {1,2,3,4,5};
        Integer target = 6;

        List<Integer> list = Arrays.asList(numbers);
        List<Integer> answer = new ArrayList<>();

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i<list.size(); i++) {
            if (map.containsKey(list.get(i))) {
                answer.add(map.get(list.get(i)));
                answer.add(i);
            } else {
                Integer difference = target - list.get(i);
                map.put(difference, i);
            }
        }
        System.out.println(answer);
    }
}